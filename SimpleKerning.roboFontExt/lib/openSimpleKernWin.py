from simpleKerning.kernWindow import SimpleKerningWindow

f = CurrentFont()
k = SimpleKerningWindow()
k.setFont(f)
k.setPairList(sorted(f.kerning.keys()))
