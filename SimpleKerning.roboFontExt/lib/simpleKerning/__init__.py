import re
from simpleKerning.constants import *
from AppKit import NSApp
import itertools
import warnings

def isKerningGroup(entry):
	"""
	>>> isKerningGroup('public.kern1.A')
	True
	>>> isKerningGroup('public.kern2.AE')
	True
	>>> isKerningGroup('a')
	False
	"""
	if re.match(RE_GROUP_TAG, entry) is None:
		return False
	return True

def getGlyphToGroupDictionary(groups):
	"""
	Only works for kerning groups.

	>>> getGlyphToGroupDictionary(testGroup) == {'A': {'public.kern1.A'},
	...											'Aacute': {'public.kern1.A'},
	...											'Acircumflex': {'public.kern1.A'},
	...											't': {'public.kern2.t'},
	...											'tcaron': {'public.kern2.t'}}
	True
	"""
	result = {}
	for group, members in groups.items():
		if isKerningGroup(group):
			for glyph in members:
				result.setdefault(glyph, set()).add(group)
	return result

def glyphToGroupWithSide(glyphName, glyphToGroupMap, side):
	"""
	Side is 1 or 0.

	>>> glyphToGroupWithSide('A', getGlyphToGroupDictionary(testGroup), 0)
	'public.kern1.A'
	>>> glyphToGroupWithSide('A', getGlyphToGroupDictionary(testGroup), 1) is None
	True
	>>> glyphToGroupWithSide('t', getGlyphToGroupDictionary(testGroup), 1)
	'public.kern2.t'
	>>> glyphToGroupWithSide('b', getGlyphToGroupDictionary(testGroup), 0) is None
	True
	"""
	sideTag = GROUP_SIDE_TAG[side]
	assert side in (0, 1)
	for groupName in glyphToGroupMap.get(glyphName, []):
		if groupName.startswith(sideTag):
			return groupName

def getSideAndRawGroupName(group_name):
	"""
	>>> getSideAndRawGroupName('public.kern1.A')
	(0, 'A')
	>>> getSideAndRawGroupName('public.kern2.B')
	(1, 'B')
	>>> getSideAndRawGroupName('a') is None
	True
	"""
	match = re.match(RE_GROUP_TAG, group_name)
	if match is not None:
		side = match.group(0)
		return GROUP_SIDE_TAG.index(side), re.split(RE_GROUP_TAG, group_name)[-1]

def getRawGroupName(name):
	"""
	>>> getRawGroupName('public.kern1.A')
	'A'
	>>> getRawGroupName('public.kern2.ae')
	'ae'
	>>> getRawGroupName('a') is None
	True
	"""
	if name is not None and isKerningGroup(name):
		return name[13:]

def getGroupSideToGroupRawName(groups):
	"""
	>>> getGroupSideToGroupRawName(testGroup) == {0: {'A'}, 1: {'t'}}
	True
	"""
	side_members = {0: set(), 1: set()}
	for group, members in groups.items():
		sideName = getSideAndRawGroupName(group)
		if sideName is not None:
			side, groupRawName = sideName
			side_members[side].add(groupRawName)
	return side_members

def flattenPair(pair, groups):
	"""
	Flatten a kerning pair.
	"""
	left, right = pair
	leftGlyphs = groups.get(left, [left])
	rightGlyphs = groups.get(right, [right])
	return list(itertools.product(leftGlyphs, rightGlyphs))

def unicodeStringToGlyphNames(text, uniValueToGlyphName):
	"""
	uniValueToGlyphName is a dict in which keys are int value and values are a
	glyphname stirng.
	"""
	def _charToGlyphName(char):
		return uniValueToGlyphName.get(ord(char), False)
	filtered = filter(_charToGlyphName, text)
	return map(_charToGlyphName, filtered)

def beautifyPair(pair):
	"""
	Remove group kerning prefix from pair name.
	"""
	result = []
	for entry in pair:
		rawName = getRawGroupName(entry)
		if rawName is not None:
			result.append("@"+rawName)
		else:
			result.append(rawName)
	return result

def isKerningPairValid(pair, glyphSet, groupSet):
	for i, e in enumerate(pair):
		match = re.match(RE_GROUP_TAG, e)
		if match is not None:
			side = match.group(0)
			if GROUP_SIDE_TAG.index(side) != i:
				warnings.warn(f"Invalid group side in pair:{pair}")
				return False
	validEntries = set(glyphSet) | set(groupSet)
	for entry in pair:
		if entry not in validEntries:
			warnings.warn(f"Missing group/glyph '{entry}' in pair: {str(pair)}")
			return False
	return True

def getWindow(windowKey):
	for w in NSApp().windows():
		delegate = w.delegate()
		if delegate:
			if not hasattr(delegate, "vanillaWrapper"):
				continue
			vanillaWrapper = delegate.vanillaWrapper()
			if hasattr(vanillaWrapper, "__name__") and vanillaWrapper.__name__ == windowKey:
				return vanillaWrapper

if __name__ == '__main__':
	import doctest
	testGroup = {
	'public.kern1.A': ("A", "Aacute", "Acircumflex"),
	'public.kern2.t': ("t", "tcaron"),
	'u': ("n"),
	}
	doctest.testmod()
