from vanilla import *
from mojo.extensions import getExtensionDefault, setExtensionDefault
from mojo.events import addObserver, removeObserver
from mojo.drawingTools import *
from mojo.UI import setGlyphViewDisplaySettings, getGlyphViewDisplaySettings, CurrentWindow, getDefault, UpdateCurrentGlyphView
import weakref
from simpleKerning import *
from mojo.roboFont import OpenWindow, CurrentFont, CurrentGlyph

numberOfGlyphs2Opacity = [1, 0.6838, 0.5358, 0.4377, 0.369, 0.3187, 0.2803, 0.2501, 0.2257, 0.2057, 0.1889, 0.1746, 0.1623, 0.1517, 0.1423, 0.134, 0.1267, 0.1201, 0.1141, 0.1087, 0.1038, 0.0994, 0.0953, 0.0915, 0.088, 0.0848, 0.0817, 0.0789, 0.0763, 0.0739, 0.0716, 0.0694, 0.0674, 0.0655, 0.0637, 0.062, 0.0603, 0.0588, 0.0573, 0.0559, 0.0546, 0.0533, 0.0521, 0.051, 0.0499, 0.0488, 0.0478, 0.0468, 0.0459, 0.045]
r, g, b, a = tuple(getDefault("glyphViewFillColor"))
RGB_F = (r, g, b)
"""
todo:
- Swap the group side on rtl click?
- Update the combo list items based on rtl or ltr

Notes:
- When the direction is right to left, the group tag is swapped.
That's what made this so compilcated! Examples:
public.kern1.beFina
public.kern2.beInit
public.kern2.Aleft
public.kern1.Aright

"""

class SKGroupWindow(object):
	__name__ = GROUP_WINDOW_KEY

	def __init__(self):

		# set the veiw settings
		previousViewSettings = getExtensionDefault('design.bahman.viewGlyphGroups.glyphViewDisplaySettings', fallback=None)
		if previousViewSettings is None:
			setExtensionDefault('design.bahman.viewMarks.GlyphViewDisplaySettings', getGlyphViewDisplaySettings())
		else:
			setGlyphViewDisplaySettings(previousViewSettings)
		self.w = HUDFloatingWindow((400, 10), "Grouping Window", maxSize=(400, 10))
		self.w.sidesSegmentedButton = SegmentedButton("auto", [dict(title="◀"), dict(title="▶")],
										callback=self._sideClicked)
		self.w.sidesSegmentedButton.set(0)
		self.w.rtlCheckBox = CheckBox("auto", "Right To Left",
								callback=self._RTLcallback, value=False)
		self.w.rightGroupNameInput = ComboBox("auto", [], callback=self._rightGroupNameInputCallback)
		self.w.leftGroupNameInput = ComboBox("auto", [], callback=self._leftGroupNameInputCallback)

		metrics = dict(
			border=15,
			largeBorder=100,
			)

		constrains = [
			"H:|-[rtlCheckBox]-|",
			"H:|-[sidesSegmentedButton]-|",
			"H:|-[leftGroupNameInput][rightGroupNameInput(==leftGroupNameInput)]-|",
			"V:|-[sidesSegmentedButton]-[leftGroupNameInput]-[rtlCheckBox]-|",
			"V:|-[sidesSegmentedButton]-[rightGroupNameInput]-[rtlCheckBox]-|",
		]

		self.w.addAutoPosSizeRules(constrains, metrics=metrics)
		self.w.vanillaWrapper = weakref.ref(self)
		self.w.center()

		self._targetGlyphNames = []  # current glyph or selected ones
		self._inputCurrentSide = 0  # left
		self._inputLeftGroupName = None
		self._inputRightGroupName = None
		self._prevGroupName = None
		self._glyphAndOffsetsToDraw = {}
		self._glyphRTLMap = {}  # "0", "1"
		self._rtl = 0
		self.setFont(CurrentFont())
		addObserver(self, '_fontBecameCurrent', 'fontBecameCurrent')
		addObserver(self, '_drawBackground', 'drawBackground')
		addObserver(self, '_viewDidChangeGlyph', 'viewDidChangeGlyph')
		addObserver(self, '_currentGlyphChanged', 'currentGlyphChanged')
		self.w.open()
		self.w.bind('close', self._removeObservers)
		self.w.makeKey()
		self._updateWindow()
		self._refreshGlyphsToDraw()

	@property
	def currentGroupName(self):
		if self._inputCurrentSide == 1:
			return self.rightGroupName
		return self.leftGroupName

	@property
	def side(self):
		if self._rtl:
			return self._inputCurrentSide
		return abs(self._inputCurrentSide - 1)

	@property
	def leftGroupName(self):
		if self._inputLeftGroupName is not None:
			rawName = self._inputLeftGroupName.strip()
			if len(rawName) > 0:
				return GROUP_SIDE_TAG[abs(self._rtl - 1)] + rawName

	@leftGroupName.setter
	def leftGroupName(self, name):
		"""
		Name should start with a tag
		"""
		self._inputLeftGroupName = getRawGroupName(name)
		self.w.leftGroupNameInput.set(self._inputLeftGroupName)
		self._refreshGlyphsToDraw()

	@property
	def rightGroupName(self):
		if self._inputRightGroupName is not None:
			rawName = self._inputRightGroupName.strip()
			if len(rawName) > 0:
				return GROUP_SIDE_TAG[self._rtl] + rawName

	@rightGroupName.setter
	def rightGroupName(self, name):
		"""
		Name should start with a tag
		"""
		self._inputRightGroupName = getRawGroupName(name)
		self.w.rightGroupNameInput.set(self._inputRightGroupName)
		self._refreshGlyphsToDraw()

	def _sideClicked(self, info):
		self._inputCurrentSide = info.get()  # 0 = left, 1 = right
		self._refreshGlyphsToDraw()

	def _RTLcallback(self, info):
		# also swap the glyph side
		self._rtl = info.get()  # if it's RTL, then RTL == 1
		for g in self._targetGlyphNames:
			self.f[g].lib[RTL_KEY] = self._rtl
		self._refreshGlyphsToDraw()

	def _rightGroupNameInputCallback(self, sender):
		self._inputCurrentSide = 1
		self.w.sidesSegmentedButton.set(1)
		self._prevGroupName = self.currentGroupName
		self._inputRightGroupName = sender.get()
		self._currentGlyphChanged()
		self._setGroupNameForTargetGlyphs()
		self._refreshGlyphsToDraw()

	def _leftGroupNameInputCallback(self, sender):
		self._inputCurrentSide = 0
		self.w.sidesSegmentedButton.set(0)
		self._prevGroupName = self.currentGroupName
		self._inputLeftGroupName = sender.get()
		self._currentGlyphChanged()
		self._setGroupNameForTargetGlyphs()
		self._refreshGlyphsToDraw()

	def _setGroupNameForTargetGlyphs(self):
		newName = self.currentGroupName
		if self.w._window.isKeyWindow() and len(self._targetGlyphNames) > 0 and newName != self._prevGroupName:
			refresh = False
			# delete the previous group membership
			for g in self._targetGlyphNames:
				prevGroupName = glyphToGroupWithSide(g, self.glyphToGroupDictionary, self.side)
				if prevGroupName is not None:
					prevMembers = list(self.f.groups.get(prevGroupName, None))
					if prevMembers is not None:
						prevMembers.remove(g)
						if len(prevMembers) > 0:
							self.f.groups[prevGroupName] = tuple(prevMembers)
						else:
							del self.f.groups[prevGroupName]
						refresh = True
			# assign new membership
			if newName is not None and newName != '':
				refresh = True
				members = set(self._targetGlyphNames)
				glyphOrder = self.f.glyphOrder
				if self.currentGroupName in self.f.groups:
					members.update(self.f.groups[newName])
				self.f.groups[newName] = sorted(members, key=lambda g: glyphOrder.index(g))
			if refresh:
				self.f.groups.changed()
				self._groupsChanged()

	def _groupsChanged(self):
		self.glyphToGroupDictionary = getGlyphToGroupDictionary(self.f.groups)
		self._groupSideToRawGroupNames = getGroupSideToGroupRawName(self.f.groups)
		self._refreshGlyphsToDraw()

	def _fontBecameCurrent(self, info):
		self.setFont(info['font'])

	def setFont(self, font):
		# set default variables
		self.f = font
		self._currentGlyph = CurrentGlyph()
		self._targetGlyphNames = []
		self._glyphRTLMap = font.rtlMap  # "0", "1"
		self._groupsChanged()

	def _drawBackground(self, info):
		try:
			opacity = numberOfGlyphs2Opacity[len(self._glyphAndOffsetsToDraw)-1]
		except IndexError:
			opacity = numberOfGlyphs2Opacity[-1]

		for glyph, offset in self._glyphAndOffsetsToDraw.items():
			save()
			fill(*RGB_F, opacity)
			translate(offset, 0)
			drawGlyph(glyph)
			restore()

	def _viewDidChangeGlyph(self, info):
		self._currentGlyph = info['glyph']
		if self._currentGlyph is not None:
			font = self._currentGlyph.font
			if font!= self.f:
				self.setFont(font)
			self._refreshGlyphsToDraw()

	def _refreshGlyphsToDraw(self):
		if self._currentGlyph is not None:
			self._glyphAndOffsetsToDraw = {}
			groupName = self.currentGroupName
			if groupName is not None:
				glyphsToDraw = self.f.groups.get(groupName, [])
				for g in glyphsToDraw:
					glyph = self.f[g]
					offset = 0
					if self.side == self._rtl:
						offset = self._currentGlyph.width - glyph.width
					self._glyphAndOffsetsToDraw[glyph] = offset
			UpdateCurrentGlyphView()

	@property
	def _rfWindowName(self):
		return CurrentWindow().doodleWindowName

	def _updateWindow(self):
		leftGroups = set()
		rightGroups = set()
		rtl = set()
		self.rightGroupName = None
		self.leftGroupName = None
		if self._targetGlyphNames:
			for gl in self._targetGlyphNames:
				groups = self.glyphToGroupDictionary.get(gl, [])
				leftAdded, rightAdded = False, False
				for gr in groups:
					if isKerningGroup(gr):
						if gr.startswith(GROUP_SIDE_TAG[0]):
							rightGroups.add(gr)
							rightAdded = True
						elif gr.startswith(GROUP_SIDE_TAG[1]):
							leftGroups.add(gr)
							leftAdded = True
				if not rightAdded:
					rightGroups.add(None)
				if not leftAdded:
					leftGroups.add(None)
				rtl.add(self.f[gl].lib.get(RTL_KEY, self._glyphRTLMap[gl]))
			self._rtl = list(rtl)[0]
			sides = (0, 1)
			if self._rtl:
				rightGroups, leftGroups = leftGroups, rightGroups
				sides = (1, 0)
			if len(rightGroups) == 1:
				self.rightGroupName = list(rightGroups)[0]
			if len(leftGroups) == 1:
				self.leftGroupName = list(leftGroups)[0]
			self.w.leftGroupNameInput.setItems(sorted(self._groupSideToRawGroupNames[sides[0]]))
			self.w.rightGroupNameInput.setItems(sorted(self._groupSideToRawGroupNames[sides[1]]))
		self.w.rtlCheckBox.set(self._rtl)

	def _currentGlyphChanged(self, info=None):
		prevTargetGlyphNames = self._targetGlyphNames[:]
		self._targetGlyphNames = []
		if self._rfWindowName == 'FontWindow':
			self._targetGlyphNames = list(self.f.selectedGlyphNames)
		elif self._rfWindowName == 'GlyphWindow':
			self._targetGlyphNames = [self._currentGlyph.name, ]
		if prevTargetGlyphNames != self._targetGlyphNames:
			self._updateWindow()

	def _removeObservers(self, sender):
		removeObserver(self, 'fontBecameCurrent')
		removeObserver(self, 'drawBackground')
		removeObserver(self, 'viewDidChangeGlyph')
		removeObserver(self, 'currentGlyphChanged')

def SimpleGroupWindow():
	OpenWindow(SKGroupWindow)
	return getWindow(GROUP_WINDOW_KEY)

if __name__ == "__main__":
	from simpleKerning import constants
	from simpleKerning import groupWindow
	from simpleKerning import pairListBuilder
	from simpleKerning.startup import *
	import simpleKerning
	from importlib import reload
	reload(constants)
	reload(simpleKerning)
	reload(groupWindow)
	reload(pairListBuilder)
	from simpleKerning.groupWindow import SimpleGroupWindow

	f = CurrentFont()
	g = SimpleGroupWindow()
