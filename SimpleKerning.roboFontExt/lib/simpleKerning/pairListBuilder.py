"""
This script creates kerning pairs for Arabic font. The font should be
fontparts object. The scipt will make all possible combiantion according to
following ciriteria:

1. Glyph types are:	**ANY**: base, base.init, base.isol, base.medi, base.fina
					**VARED**: base, base.isol, base.fina
3. Typical pair: (VARED, ANY): value

Bahman Eslami Nov. 2019
"""
import itertools
from simpleKerning import *

def getBaseFromLastSuffix(glyphName, suffixes):
	"""
	Return base and the last found suffix and ignore the remainging suffixes.

	>>> getBaseFromLastSuffix("alef.fina", ["fina"])
	('alef', 'fina')
	>>> getBaseFromLastSuffix("lam.init_alef.fina.liga", ["fina", "init", "medi"])
	('lam.init_alef', 'fina')
	"""
	if '.' in glyphName:
		splitted = glyphName.split('.')
		i = -1
		while True:
			#  trying to eliminate non arabic suffixes until we reach the
			#  arabic suffix
			try:
				suffix = splitted[i]
			except IndexError:
				return
			if suffix in suffixes:
				base = '.'.join(splitted[:i])
				return base, suffix
			i -= 1

def seperateBaseAndSuffix(glyphName, suffixes):
	"""
	Return base and all the found suffixes seperately.

	>>> seperateBaseAndSuffix("lam.init_alef.fina.liga", ["fina", "init", "medi"])
	(('lam', 'alef'), ('init', 'fina'))
	>>> seperateBaseAndSuffix("A.case", ["case"])
	(('A',), ('case',))
	"""
	outSuffixes = []
	bases = []
	# in ligature get suffix of components
	comps = glyphName.split('_')
	for c in comps:
		b_s = getBaseFromLastSuffix(c, suffixes)
		if b_s is not None:
			base, suffix = b_s
			outSuffixes.append(suffix)
			bases.append(base)
	return tuple(bases), tuple(outSuffixes)

def getBaseAndSuffixDict(glyphNames, suffixes):
	"""
	>>> baseDict, glyphNameToBaseAndSuffix = getBaseAndSuffixDict(["alef.fina", "alef"], ("fina"))
	>>> baseDict == {'alef': {'fina'}}
	True
	>>> glyphNameToBaseAndSuffix == {'alef.fina': (('alef',), ('fina',)), 'alef': ((), ())}
	True
	"""
	baseToSuffixes = {}  # beh: set(['fina', 'medi', 'isol', 'init'])
	glyphNameToBaseAndSuffix = {}  # lam.init_alef.fina.liga: ((lam, alef), (init, fina))

	glyphNames = set(glyphNames)

	for glyphName in glyphNames:
		baseAndSuffix = seperateBaseAndSuffix(glyphName, suffixes)
		bases, glyphSuffixes = baseAndSuffix
		if set(bases) - glyphNames == set():  # all bases exist in glyphNames
			for base, suffix in zip(bases, glyphSuffixes):
				baseToSuffixes.setdefault(base, set()).add(suffix)
			glyphNameToBaseAndSuffix[glyphName] = baseAndSuffix
	return baseToSuffixes, glyphNameToBaseAndSuffix

def filterToSuffixes(glyphSet, allGlyphs, suffixes):
	"""
	filter the glyphSet to suffixes from allGlyphs.
	glyphSet: ['A']
	allGlyphs: ['A', 'A.case']
	suffixes: ['case']
	returns: ['A.case']

	>>> filterToSuffixes(["A"], ["A", "A.case"], ["case"])
	{'A.case'}
	>>> filterToSuffixes(["A"], ["A", "A.case"], ["fina"])
	set()
	>>> filterToSuffixes(["alef"], ["alef", "lam", "lam.init", "alef.fina", "lam.init_alef.fina.liga"], ["fina"]) == {'alef.fina', 'lam.init_alef.fina.liga'}
	True
	"""
	# result = set(glyphNames)
	glyphSet = set(glyphSet)
	suffixes = set(suffixes)
	result = set()
	baseToSuffixes, glyphNameToBaseAndSuffix = getBaseAndSuffixDict(allGlyphs, suffixes)
	for glyphName, baseAndSuffix in glyphNameToBaseAndSuffix.items():
		this_bases, this_suffixes = baseAndSuffix
		if set(this_bases) - glyphSet == set() and suffixes & set(this_suffixes) != set():
			result.add(glyphName)
	return result

def compressKerningPair(kerningPair, glyphToGroupMap):
	"""
	Convert glyphs to groups in kerning pair.
	"""
	result = []
	for side, glyph in enumerate(kerningPair):
		entry = glyphToGroupWithSide(glyph, glyphToGroupMap, side)
		if entry is None:
			entry = glyph
		result.append(entry)
	return tuple(result)

def compressPairList(kerningPairList, glyphToGroupMap):
	"""
	Compress flat kerning pair list to group kerning pair list.
	"""
	return sorted(set(map(lambda pair: compressKerningPair(pair, glyphToGroupMap), kerningPairList)))

class PairList(list):

	def __init__(self, font=None, name="", *args, **kwargs):
		super().__init__(*args, **kwargs)
		if font is not None:
			self.setFont(font)
		self.name = name
		self._instructions = {}
		self._sortedSide = 0
		self._beforeContext = ""
		self._afterContext = ""
		self._referenceText = ""

	def setFont(self, font):
		self.font = font
		self.glyphNames = font.keys()
		self._glyphToGroupMap = getGlyphToGroupDictionary(font.groups)
		self._uniValueToGlyphName = {u: v[0] for u, v in font.getCharacterMapping().items()}

	def _isKerningPairValid(self, pair):
		return isKerningPairValid(pair, self.glyphNames, self.font.groups.keys())

	def fromText(self, first, second, firstSuffixes=[], secondSuffixes=[], compress=True):
		self._instructions.setdefault("fromText", []).append(self._argsToStr(first, second, firstSuffixes, secondSuffixes, compress))
		firstGlyphs = unicodeStringToGlyphNames(first, self._uniValueToGlyphName)
		secondGlyphs = unicodeStringToGlyphNames(second, self._uniValueToGlyphName)
		if firstSuffixes:
			firstGlyphs = filterToSuffixes(firstGlyphs, self.glyphNames, firstSuffixes)
		if secondSuffixes:
			secondGlyphs = filterToSuffixes(secondGlyphs, self.glyphNames, secondSuffixes)
		allCombinations = itertools.product(firstGlyphs, secondGlyphs)
		if compress:
			allCombinations = compressPairList(allCombinations, self._glyphToGroupMap)
		self.extend([p for p in allCombinations if p not in self])
		self.sort()
		return self

	def fromList(self, pairList):
		# add only valid entries
		self.extend(filter(self._isKerningPairValid, pairList))

	def addPair(self, pair, index=0):
		# mainly for adding exceptions
		if self._isKerningPairValid(pair):
			self.insert(index, pair)

	def removePairAtIndex(self, index):
		del self[index]

	def clear(self):
		super().clear()
		self._instructions = {}

	def sortBySide(self, side=None):
		if side is None:
			self._sortedSide = abs(self._sortedSide - 1)
		else:
			self._sortedSide = side
		self.sort(key=lambda x: x[self._sortedSide])

	def _argsToStr(self, *args, **kwargs):
		return str(args), str(kwargs)

	def setBeforeContext(self, context):
		self._instructions["setBeforeContext"] = [self._argsToStr(context)]
		self._beforeContext = context

	def setAfterContext(self, context):
		self._instructions["setAfterContext"] = [self._argsToStr(context)]
		self._afterContext = context

	def getBeforeContext(self):
		return self._beforeContext

	def getAfterContext(self):
		return self._afterContext

	afterContext = property(getAfterContext, setAfterContext)
	beforeContext = property(getBeforeContext, setBeforeContext)

	def setReferenceText(self, txt):
		self._instructions["setReferenceText"] = [self._argsToStr(txt)]
		self._referenceText = txt

	def getReferenceText(self):
		return self._referenceText

	referenceText = property(getReferenceText, setReferenceText)

	def loadInstructions(self, instrucions):
		for funcName, argKwargList in instrucions.items():
			func = getattr(self, funcName)
			for args, kwargs in argKwargList:
				func(*eval(args), **eval(kwargs))

	def getInstructions(self):
		return self._instructions

def _testPairLists():
	"""
	>>> PairList(testFont).fromText("زر", "بل", firstSuffixes=['fina'], secondSuffixes=['init'])
	[('reh.fina', 'beh.init'), ('reh.fina', 'lam.init'), ('reh.fina', 'lam.init_ah.fina'), ('reh.fina', 'lam.init_alef.fina'), ('reh.fina', 'lam.init_alefHamzeAbove.fina'), ('reh.fina', 'lam.init_alefHamzeBelow.fina'), ('zeh.fina', 'beh.init'), ('zeh.fina', 'lam.init'), ('zeh.fina', 'lam.init_ah.fina'), ('zeh.fina', 'lam.init_alef.fina'), ('zeh.fina', 'lam.init_alefHamzeAbove.fina'), ('zeh.fina', 'lam.init_alefHamzeBelow.fina')]
	"""

if __name__ == '__main__':
	import doctest
	from fontParts.fontshell import RFont

	testGlyphs = ['.notdef', '.null', 'A', 'AE', 'Aacute', 'Abreve', 'Acircumflex', 'Adieresis', 'Agrave', 'Amacron', 'Aogonek', 'Aring', 'Atilde', 'B', 'C', 'CR', 'Cacute', 'Ccaron', 'Ccedilla', 'Ccircumflex', 'Cdotaccent', 'D', 'Dcaron', 'Dcroat', 'Delta', 'DotAbove', 'DotBelow', 'E', 'Eacute', 'Ebreve', 'Ecaron', 'Ecircumflex', 'Edieresis', 'Edotaccent', 'Egrave', 'Emacron', 'Eng', 'Eogonek', 'Eth', 'Euro', 'F', 'G', 'Gbreve', 'Gcircumflex', 'Gcommaaccent', 'Gdotaccent', 'H', 'Hbar', 'Hcircumflex', 'I', 'IJ', 'Iacute', 'Ibreve', 'Icircumflex', 'Idieresis', 'Idotaccent', 'Igrave', 'Imacron', 'Iogonek', 'Itilde', 'J', 'Jcircumflex', 'K', 'Kcommaaccent', 'L', 'Lacute', 'Lcaron', 'Lcommaaccent', 'Ldot', 'Lslash', 'M', 'N', 'Nacute', 'Ncaron', 'Ncommaaccent', 'Ntilde', 'O', 'OE', 'Oacute', 'Obreve', 'Ocircumflex', 'Odieresis', 'Ograve', 'Ohungarumlaut', 'Omacron', 'Omega', 'Oslash', 'Otilde', 'P', 'Q', 'R', 'Racute', 'Rcaron', 'Rcommaaccent', 'S', 'Sacute', 'Scaron', 'Scedilla', 'Scircumflex', 'T', 'Tbar', 'Tcaron', 'Thorn', 'U', 'Uacute', 'Ubreve', 'Ucircumflex', 'Udieresis', 'Ugrave', 'Uhungarumlaut', 'Umacron', 'Uogonek', 'Uring', 'Utilde', 'V', 'W', 'Wacute', 'Wcircumflex', 'Wdieresis', 'Wgrave', 'X', 'Y', 'Yacute', 'Ycircumflex', 'Ydieresis', 'Ygrave', 'Z', 'Zacute', 'Zcaron', 'Zdotaccent', 'a', 'a.open', 'aacute', 'aacute.open', 'abreve', 'abreve.open', 'acircumflex', 'acircumflex.open', 'acute', 'adieresis', 'adieresis.open', 'ae', 'afii10017', 'afii10018', 'afii10019', 'afii10020', 'afii10021', 'afii10022', 'afii10023', 'afii10024', 'afii10025', 'afii10026', 'afii10027', 'afii10028', 'afii10029', 'afii10030', 'afii10031', 'afii10032', 'afii10033', 'afii10034', 'afii10035', 'afii10036', 'afii10037', 'afii10038', 'afii10039', 'afii10040', 'afii10041', 'afii10042', 'afii10043', 'afii10044', 'afii10045', 'afii10046', 'afii10047', 'afii10048', 'afii10049', 'afii10050', 'afii10051', 'afii10052', 'afii10053', 'afii10054', 'afii10055', 'afii10056', 'afii10057', 'afii10058', 'afii10059', 'afii10060', 'afii10061', 'afii10062', 'afii10065', 'afii10066', 'afii10067', 'afii10068', 'afii10069', 'afii10070', 'afii10071', 'afii10072', 'afii10073', 'afii10074', 'afii10075', 'afii10076', 'afii10077', 'afii10078', 'afii10079', 'afii10080', 'afii10081', 'afii10082', 'afii10083', 'afii10084', 'afii10085', 'afii10086', 'afii10087', 'afii10088', 'afii10089', 'afii10090', 'afii10091', 'afii10092', 'afii10093', 'afii10094', 'afii10095', 'afii10096', 'afii10097', 'afii10098', 'afii10099', 'afii10100', 'afii10101', 'afii10102', 'afii10103', 'afii10104', 'afii10105', 'afii10106', 'afii10107', 'afii10108', 'afii10109', 'afii10110', 'afii10145', 'afii10146', 'afii10147', 'afii10148', 'afii10193', 'afii10194', 'afii10195', 'afii10196', 'agrave', 'agrave.open', 'ah', 'ah.fina', 'ah.isol', 'alef', 'alef.fina', 'alef.isol', 'alefHamzeAbove', 'alefHamzeAbove.fina', 'alefHamzeAbove.isol', 'alefHamzeBelow', 'alefHamzeBelow.fina', 'alefHamzeBelow.isol', 'alefMaghsureh', 'alefMaghsureh.fina', 'alefMaghsureh.isol', 'alefShort.fina', 'alefShort.isol', 'alefWasleh', 'alefWasleh.fina', 'alefWasleh.isol', 'alefWavyHamzehAbove', 'alefWavyHamzehAbove.fina', 'amacron', 'amacron.open', 'ampersand', 'aogonek', 'aogonek.open', 'approxequal', 'arabicDateSeperator', 'arabicPercent', 'aring', 'aring.open', 'arrowdown', 'arrowleft', 'arrowright', 'arrowup', 'asciicircum', 'asciitilde', 'asterisk', 'at', 'at.case', 'atilde', 'atilde.open', 'b', 'backslash', 'bar', 'bar.case', 'beh', 'beh.fina', 'beh.init', 'beh.isol', 'beh.medi', 'braceleft', 'braceleft.case', 'braceright', 'braceright.case', 'bracketleft', 'bracketleft.case', 'bracketright', 'bracketright.case', 'breve', 'breve_cyr', 'brokenbar', 'brokenbar.case', 'bullet', 'c', 'cacute', 'caron', 'caron.alt', 'ccaron', 'ccedilla', 'ccircumflex', 'cdotaccent', 'cedilla', 'cent', 'char', 'charArabic', 'cheh', 'cheh.fina', 'cheh.init', 'cheh.isol', 'cheh.medi', 'circumflex', 'colon', 'comma', 'commaArabic', 'commaaccent', 'copyright', 'currency', 'd', 'dagger', 'daggerdbl', 'dal', 'dal.fina', 'dal.isol', 'dcaron', 'dcroat', 'degree', 'dieresis', 'divide', 'do', 'doArabic', 'dollar', 'dotaccent', 'dotlessBeh', 'dotlessBeh.fina', 'dotlessBeh.init', 'dotlessBeh.medi', 'dotlessFeh', 'dotlessFeh.fina', 'dotlessFeh.init', 'dotlessFeh.medi', 'dotlessi', 'dotlessj', 'e', 'e.open', 'eacute', 'eacute.open', 'ebreve', 'ebreve.open', 'ecaron', 'ecaron.open', 'ecircumflex', 'ecircumflex.open', 'edieresis', 'edieresis.open', 'edotaccent', 'edotaccent.open', 'egrave', 'egrave.open', 'eight', 'eight.denominator', 'eight.numerator', 'eight.posf', 'eightinferior', 'eightninths', 'eightsuperior', 'ellipsis', 'emacron', 'emacron.open', 'emdash', 'emdash.case', 'endash', 'endash.case', 'eng', 'eogonek', 'eogonek.open', 'equal', 'estimated', 'eth', 'exclam', 'exclamdown', 'exclamdown.case', 'eyn', 'eyn.fina', 'eyn.init', 'eyn.isol', 'eyn.medi', 'f', 'f_b', 'f_f_b', 'f_f_f', 'f_f_h', 'f_f_j', 'f_f_k', 'f_f_l', 'f_h', 'f_k', 'fatheh', 'fathehTanvin', 'feh', 'feh.fina', 'feh.init', 'feh.isol', 'feh.medi', 'fehThreeDotAbove', 'fehThreeDotAbove.fina', 'fehThreeDotAbove.init', 'fehThreeDotAbove.isol', 'fehThreeDotAbove.medi', 'ff', 'ffi', 'fi', 'five', 'five.denominator', 'five.numerator', 'five.posf', 'fiveeighths', 'fiveinferior', 'fiveninths', 'fivesevenths', 'fivesixths', 'fivesuperior', 'fj', 'fl', 'florin', 'four', 'four.denominator', 'four.numerator', 'four.posf', 'fourfifths', 'fourinferior', 'fourninths', 'foursevenths', 'foursuperior', 'fraction', 'g', 'g.open', 'gaf', 'gaf.fina', 'gaf.init', 'gaf.isol', 'gaf.medi', 'gbreve', 'gbreve.open', 'gcircumflex', 'gcircumflex.open', 'gcommaaccent', 'gcommaaccent.open', 'gdotaccent', 'gdotaccent.open', 'germandbls', 'ghaf', 'ghaf.fina', 'ghaf.init', 'ghaf.isol', 'ghaf.medi', 'gheyn', 'gheyn.fina', 'gheyn.init', 'gheyn.isol', 'gheyn.medi', 'grave', 'greater', 'greaterequal', 'guillemotleft', 'guillemotleft.case', 'guillemotright', 'guillemotright.case', 'guilsinglleft', 'guilsinglleft.case', 'guilsinglright', 'guilsinglright.case', 'h', 'haft', 'haftArabic', 'hah', 'hah.fina', 'hah.init', 'hah.isol', 'hah.medi', 'hamze', 'hamzehAbove', 'hamzehAbove_fatheh.cmps', 'hamzehAbove_sukun.cmps', 'hamzehAbove_tashdid.cmps', 'hamzehAbove_zameh.cmps', 'hamzehAbove_zamehTanvin.cmps', 'hamzehBelow', 'hamzehBelow_kasreh.cmps', 'hamzehBelow_kasrehTanvin.cmps', 'hasht', 'hashtArabic', 'hbar', 'hcircumflex', 'heh', 'heh.fina', 'heh.init', 'heh.isol', 'heh.medi', 'hehIsol', 'hezargan', 'hungarumlaut', 'hyphen', 'hyphen.case', 'i', 'i.dot', 'iacute', 'ibreve', 'icircumflex', 'idieresis', 'igrave', 'ij', 'imacron', 'infinity', 'integral', 'iogonek', 'itilde', 'j', 'jcircumflex', 'jim', 'jim.fina', 'jim.init', 'jim.isol', 'jim.medi', 'k', 'kaf', 'kaf.fina', 'kaf.init', 'kaf.isol', 'kaf.medi', 'kafArabic', 'kafArabic.fina', 'kafArabic.init', 'kafArabic.isol', 'kafArabic.medi', 'kasreh', 'kasrehTanvin', 'kasrehTanvin_tashdid.cmps', 'kasreh_tashdid.cmps', 'kcommaaccent', 'keshide', 'kgreenlandic', 'kgreenlandic.case', 'kheh', 'kheh.fina', 'kheh.init', 'kheh.isol', 'kheh.medi', 'l', 'lacute', 'lam', 'lam.fina', 'lam.init', 'lam.init_ah.fina', 'lam.init_alef.fina', 'lam.init_alefHamzeAbove.fina', 'lam.init_alefHamzeBelow.fina', 'lam.isol', 'lam.medi', 'lam.medi_ah.fina', 'lam.medi_alef.fina', 'lam.medi_alefHamzeAbove.fina', 'lam.medi_alefHamzeBelow.fina', 'lcaron', 'lcommaaccent', 'ldot', 'less', 'lessequal', 'logicalnot', 'lozenge', 'lslash', 'm', 'macron', 'mad', 'mim', 'mim.fina', 'mim.init', 'mim.isol', 'mim.medi', 'minus', 'momayez', 'mu', 'multiply', 'n', 'nacute', 'napostrophe', 'napostrophe.case', 'ncaron', 'ncommaaccent', 'nine', 'nine.denominator', 'nine.numerator', 'nine.posf', 'nineinferior', 'ninesuperior', 'noh', 'nohArabic', 'nonBreakingSpace', 'noon', 'noon.fina', 'noon.init', 'noon.isol', 'noon.medi', 'noonGhuna', 'noonGhuna.fina', 'noonGhuna.isol', 'notequal', 'ntilde', 'numbersign', 'o', 'oacute', 'obreve', 'ocircumflex', 'odieresis', 'oe', 'ogonek', 'ograve', 'ohungarumlaut', 'omacron', 'one', 'one.denominator', 'one.numerator', 'one.posf', 'oneeighth', 'onefifth', 'onehalf', 'oneinferior', 'oneninth', 'onequarter', 'oneseventh', 'onesixth', 'onesuperior', 'onethird', 'ordfeminine', 'ordmasculine', 'oslash', 'otilde', 'ovalComponent', 'p', 'panj', 'panjArabic', 'paragraph', 'parenleft', 'parenleft.case', 'parenright', 'parenright.case', 'partialdiff', 'peh', 'peh.fina', 'peh.init', 'peh.isol', 'peh.medi', 'percent', 'period', 'periodcentered', 'perthousand', 'pi', 'plus', 'plusminus', 'product', 'q', 'question', 'questionArabic', 'questiondown', 'questiondown.case', 'quotedbl', 'quotedblbase', 'quotedblleft', 'quotedblright', 'quoteleft', 'quoteright', 'quotesinglbase', 'quotesingle', 'r', 'racute', 'radical', 'rcaron', 'rcommaaccent', 'registered', 'reh', 'reh.fina', 'reh.isol', 'revcommaaccent', 'ring', 's', 'sacute', 'sad', 'sad.fina', 'sad.init', 'sad.isol', 'sad.medi', 'scaron', 'scedilla', 'scircumflex', 'se', 'seArabic', 'section', 'sefr', 'sefrArabic', 'semicolon', 'semicolonArabic', 'seven', 'seven.denominator', 'seven.numerator', 'seven.posf', 'seveneighths', 'seveninferior', 'sevenninths', 'sevensuperior', 'shin', 'shin.fina', 'shin.init', 'shin.isol', 'shin.medi', 'shish', 'shishArabic', 'sin', 'sin.fina', 'sin.init', 'sin.isol', 'sin.medi', 'six', 'six.denominator', 'six.numerator', 'six.posf', 'sixinferior', 'sixsevenths', 'sixsuperior', 'slash', 'space', 'sterling', 'subScriptAlef', 'sukun', 'summation', 'superScriptAlef', 't', 'ta', 'ta.fina', 'ta.init', 'ta.isol', 'ta.medi', 'tashdid', 'tashdid_fatheh.cmps', 'tashdid_fathehTanvin.cmps', 'tashdid_superScriptAlef.cmps', 'tashdid_zameh.cmps', 'tashdid_zamehTanvin.cmps', 'tbar', 'tcaron', 'teh', 'teh.fina', 'teh.init', 'teh.isol', 'teh.medi', 'tehArabic', 'tehArabic.fina', 'tehArabic.isol', 'theh', 'theh.fina', 'theh.init', 'theh.isol', 'theh.medi', 'thorn', 'three', 'three.denominator', 'three.numerator', 'three.posf', 'threeDotAbove', 'threeDotBelow', 'threeeighths', 'threefifths', 'threeinferior', 'threequarters', 'threesevenths', 'threesuperior', 'tilde', 'trademark', 'two', 'two.denominator', 'two.numerator', 'two.posf', 'twoDotAbove', 'twoDotBelow', 'twofifths', 'twoinferior', 'twoninths', 'twosevenths', 'twosuperior', 'twothirds', 'u', 'uacute', 'ubreve', 'ucircumflex', 'udieresis', 'ugrave', 'uhungarumlaut', 'umacron', 'underscore', 'uni00A0', 'uni00AD', 'uni0162', 'uni0163', 'uni0218', 'uni0219', 'uni021A', 'uni021B', 'uni02BC', 'uni0400', 'uni040D', 'uni0450', 'uni045D', 'uni20BD', 'uni2113', 'uni2194', 'uni2195', 'uni2196', 'uni2197', 'uni2198', 'uni2199', 'uni2215', 'uogonek', 'uring', 'utilde', 'v', 'vav', 'vav.fina', 'vav.isol', 'vavHamzeAbove', 'vavHamzeAbove.fina', 'vavHamzeAbove.isol', 'w', 'wacute', 'wasleh', 'wavyHamzehAbove', 'wcircumflex', 'wdieresis', 'wgrave', 'x', 'y', 'yacute', 'ycircumflex', 'ydieresis', 'yeh', 'yeh.fina', 'yeh.init', 'yeh.isol', 'yeh.medi', 'yehArabic', 'yehArabic.fina', 'yehArabic.init', 'yehArabic.isol', 'yehArabic.medi', 'yehHamze', 'yehHamze.fina', 'yehHamze.init', 'yehHamze.isol', 'yehHamze.medi', 'yek', 'yekArabic', 'yen', 'ygrave', 'z', 'za', 'za.fina', 'za.init', 'za.isol', 'za.medi', 'zacute', 'zad', 'zad.fina', 'zad.init', 'zad.isol', 'zad.medi', 'zal', 'zal.fina', 'zal.isol', 'zameh', 'zamehBaraks', 'zamehTanvin', 'zcaron', 'zdotaccent', 'zeh', 'zeh.fina', 'zeh.isol', 'zero', 'zero.denominator', 'zero.numerator', 'zero.posf', 'zeroWidthJoiner', 'zeroWidthNonJoiner', 'zeroinferior', 'zerosuperior', 'zheh', 'zheh.fina', 'zheh.isol']
	testCmap = {0: ['.null'], 13: ['CR'], 32: ['space'], 160: ['nonBreakingSpace', 'uni00A0'], 65165: ['alef.isol'], 65166: ['alef.fina'], 65153: ['ah.isol'], 1619: ['mad'], 65154: ['ah.fina'], 65155: ['alefHamzeAbove.isol'], 1620: ['hamzehAbove'], 65156: ['alefHamzeAbove.fina'], 65159: ['alefHamzeBelow.isol'], 1621: ['hamzehBelow'], 65160: ['alefHamzeBelow.fina'], 1650: ['alefWavyHamzehAbove'], 64336: ['alefWasleh.isol'], 64337: ['alefWasleh.fina'], 1646: ['dotlessBeh'], 65167: ['beh.isol'], 64435: ['DotBelow'], 64434: ['DotAbove'], 65168: ['beh.fina'], 65170: ['beh.medi'], 65169: ['beh.init'], 64342: ['peh.isol'], 64439: ['threeDotBelow'], 64436: ['twoDotAbove'], 64343: ['peh.fina'], 64345: ['peh.medi'], 64344: ['peh.init'], 65173: ['teh.isol'], 65174: ['teh.fina'], 65176: ['teh.medi'], 65175: ['teh.init'], 65177: ['theh.isol'], 64438: ['threeDotAbove'], 65178: ['theh.fina'], 65180: ['theh.medi'], 65179: ['theh.init'], 65181: ['jim.isol'], 65185: ['hah.isol'], 65182: ['jim.fina'], 65186: ['hah.fina'], 65184: ['jim.medi'], 65188: ['hah.medi'], 65187: ['hah.init'], 65183: ['jim.init'], 64378: ['cheh.isol'], 64379: ['cheh.fina'], 64381: ['cheh.medi'], 64380: ['cheh.init'], 65189: ['kheh.isol'], 65190: ['kheh.fina'], 65192: ['kheh.medi'], 65191: ['kheh.init'], 65193: ['dal.isol'], 65194: ['dal.fina'], 65195: ['zal.isol'], 65196: ['zal.fina'], 65197: ['reh.isol'], 65198: ['reh.fina'], 65199: ['zeh.isol'], 65200: ['zeh.fina'], 64394: ['zheh.isol'], 64395: ['zheh.fina'], 65201: ['sin.isol'], 65202: ['sin.fina'], 65204: ['sin.medi'], 65203: ['sin.init'], 65205: ['shin.isol'], 65206: ['shin.fina'], 65208: ['shin.medi'], 65207: ['shin.init'], 65209: ['sad.isol'], 65210: ['sad.fina'], 65212: ['sad.medi'], 65211: ['sad.init'], 65213: ['zad.isol'], 65214: ['zad.fina'], 65216: ['zad.medi'], 65215: ['zad.init'], 65217: ['ta.isol'], 65218: ['ta.fina'], 65220: ['ta.medi'], 65219: ['ta.init'], 65221: ['za.isol'], 65222: ['za.fina'], 65224: ['za.medi'], 65223: ['za.init'], 65225: ['eyn.isol'], 65226: ['eyn.fina'], 65228: ['eyn.medi'], 65227: ['eyn.init'], 65229: ['gheyn.isol'], 65230: ['gheyn.fina'], 65232: ['gheyn.medi'], 65231: ['gheyn.init'], 65233: ['feh.isol'], 1697: ['dotlessFeh'], 65234: ['feh.fina'], 65236: ['feh.medi'], 65235: ['feh.init'], 64362: ['fehThreeDotAbove.isol'], 64365: ['fehThreeDotAbove.medi'], 64363: ['fehThreeDotAbove.fina'], 64364: ['fehThreeDotAbove.init'], 65237: ['ghaf.isol'], 65238: ['ghaf.fina'], 65240: ['ghaf.medi'], 65239: ['ghaf.init'], 65241: ['kafArabic.isol'], 65242: ['kafArabic.fina'], 65244: ['kafArabic.medi'], 64401: ['kaf.medi'], 65243: ['kafArabic.init'], 64400: ['kaf.init'], 64398: ['kaf.isol'], 64399: ['kaf.fina'], 64402: ['gaf.isol'], 64403: ['gaf.fina'], 64405: ['gaf.medi'], 64404: ['gaf.init'], 65245: ['lam.isol'], 65246: ['lam.fina'], 65248: ['lam.medi'], 65247: ['lam.init'], 65249: ['mim.isol'], 65250: ['mim.fina'], 65252: ['mim.medi'], 65251: ['mim.init'], 64414: ['noonGhuna.isol'], 64415: ['noonGhuna.fina'], 65253: ['noon.isol'], 65254: ['noon.fina'], 65256: ['noon.medi'], 65255: ['noon.init'], 65261: ['vav.isol'], 65262: ['vav.fina'], 65157: ['vavHamzeAbove.isol'], 65158: ['vavHamzeAbove.fina'], 65257: ['heh.isol'], 65258: ['heh.fina'], 65260: ['heh.medi'], 65259: ['heh.init'], 65171: ['tehArabic.isol'], 65172: ['tehArabic.fina'], 64508: ['yeh.isol'], 64509: ['yeh.fina'], 64511: ['yeh.medi'], 64437: ['twoDotBelow'], 64510: ['yeh.init'], 65265: ['yehArabic.isol'], 65266: ['yehArabic.fina'], 65268: ['yehArabic.medi'], 65267: ['yehArabic.init'], 65161: ['yehHamze.isol'], 65162: ['yehHamze.fina'], 65164: ['yehHamze.medi'], 65163: ['yehHamze.init'], 65263: ['alefMaghsureh.isol'], 65264: ['alefMaghsureh.fina'], 65275: ['lam.init_alef.fina'], 65276: ['lam.medi_alef.fina'], 65269: ['lam.init_ah.fina'], 65270: ['lam.medi_ah.fina'], 65271: ['lam.init_alefHamzeAbove.fina'], 65272: ['lam.medi_alefHamzeAbove.fina'], 65273: ['lam.init_alefHamzeBelow.fina'], 65274: ['lam.medi_alefHamzeBelow.fina'], 1569: ['hamze'], 1570: ['ah'], 1571: ['alefHamzeAbove'], 1573: ['alefHamzeBelow'], 1649: ['alefWasleh'], 1575: ['alef'], 1576: ['beh'], 1662: ['peh'], 1578: ['teh'], 1579: ['theh'], 1580: ['jim'], 1670: ['cheh'], 1581: ['hah'], 1582: ['kheh'], 1583: ['dal'], 1584: ['zal'], 1585: ['reh'], 1586: ['zeh'], 1688: ['zheh'], 1587: ['sin'], 1588: ['shin'], 1589: ['sad'], 1590: ['zad'], 1591: ['ta'], 1592: ['za'], 1593: ['eyn'], 1594: ['gheyn'], 1601: ['feh'], 1700: ['fehThreeDotAbove'], 1602: ['ghaf'], 1705: ['kaf'], 1711: ['gaf'], 1603: ['kafArabic'], 1604: ['lam'], 1605: ['mim'], 1722: ['noonGhuna'], 1606: ['noon'], 1608: ['vav'], 1572: ['vavHamzeAbove'], 1607: ['heh'], 1577: ['tehArabic'], 1740: ['yeh'], 1610: ['yehArabic'], 1574: ['yehHamze'], 1609: ['alefMaghsureh'], 1600: ['keshide'], 1632: ['sefrArabic'], 1633: ['yekArabic'], 1777: ['yek'], 1634: ['doArabic'], 1778: ['do'], 1635: ['seArabic'], 1779: ['se'], 1636: ['charArabic'], 1637: ['panjArabic'], 1638: ['shishArabic'], 1639: ['haftArabic'], 1783: ['haft'], 1640: ['hashtArabic'], 1784: ['hasht'], 1641: ['nohArabic'], 1785: ['noh'], 1776: ['sefr'], 1780: ['char'], 1781: ['panj'], 1782: ['shish'], 1614: ['fatheh'], 1611: ['fathehTanvin'], 1615: ['zameh'], 1612: ['zamehTanvin'], 1618: ['sukun'], 1617: ['tashdid'], 1648: ['superScriptAlef'], 64606: ['tashdid_zamehTanvin.cmps'], 64608: ['tashdid_fatheh.cmps'], 64609: ['tashdid_zameh.cmps'], 64611: ['tashdid_superScriptAlef.cmps'], 1623: ['zamehBaraks'], 1616: ['kasreh'], 1613: ['kasrehTanvin'], 1622: ['subScriptAlef'], 1548: ['commaArabic'], 1563: ['semicolonArabic'], 46: ['period'], 1567: ['questionArabic'], 1549: ['arabicDateSeperator'], 1643: ['momayez'], 1642: ['arabicPercent'], 1644: ['hezargan'], 8204: ['zeroWidthNonJoiner'], 8205: ['zeroWidthJoiner'], 65: ['A'], 66: ['B'], 67: ['C'], 68: ['D'], 69: ['E'], 70: ['F'], 71: ['G'], 72: ['H'], 73: ['I'], 74: ['J'], 75: ['K'], 76: ['L'], 77: ['M'], 78: ['N'], 79: ['O'], 80: ['P'], 81: ['Q'], 82: ['R'], 83: ['S'], 84: ['T'], 85: ['U'], 86: ['V'], 87: ['W'], 88: ['X'], 89: ['Y'], 90: ['Z'], 198: ['AE'], 338: ['OE'], 321: ['Lslash'], 216: ['Oslash'], 208: ['Eth'], 222: ['Thorn'], 97: ['a'], 98: ['b'], 99: ['c'], 100: ['d'], 101: ['e'], 102: ['f'], 103: ['g'], 104: ['h'], 105: ['i'], 106: ['j'], 107: ['k'], 108: ['l'], 109: ['m'], 110: ['n'], 111: ['o'], 112: ['p'], 113: ['q'], 114: ['r'], 115: ['s'], 116: ['t'], 117: ['u'], 118: ['v'], 119: ['w'], 120: ['x'], 121: ['y'], 122: ['z'], 230: ['ae'], 339: ['oe'], 322: ['lslash'], 248: ['oslash'], 240: ['eth'], 254: ['thorn'], 223: ['germandbls'], 305: ['dotlessi'], 567: ['dotlessj'], 64256: ['ff'], 64257: ['fi'], 64258: ['fl'], 64259: ['ffi'], 64260: ['f_f_l'], 193: ['Aacute'], 180: ['acute'], 194: ['Acircumflex'], 710: ['circumflex'], 196: ['Adieresis'], 168: ['dieresis'], 192: ['Agrave'], 96: ['grave'], 197: ['Aring'], 730: ['ring'], 195: ['Atilde'], 732: ['tilde'], 258: ['Abreve'], 728: ['breve'], 256: ['Amacron'], 175: ['macron'], 260: ['Aogonek'], 731: ['ogonek'], 199: ['Ccedilla'], 184: ['cedilla'], 262: ['Cacute'], 268: ['Ccaron'], 711: ['caron'], 264: ['Ccircumflex'], 266: ['Cdotaccent'], 729: ['dotaccent'], 270: ['Dcaron'], 272: ['Dcroat'], 201: ['Eacute'], 202: ['Ecircumflex'], 203: ['Edieresis'], 200: ['Egrave'], 276: ['Ebreve'], 282: ['Ecaron'], 278: ['Edotaccent'], 274: ['Emacron'], 280: ['Eogonek'], 286: ['Gbreve'], 284: ['Gcircumflex'], 290: ['Gcommaaccent'], 63171: ['commaaccent'], 288: ['Gdotaccent'], 294: ['Hbar'], 292: ['Hcircumflex'], 205: ['Iacute'], 206: ['Icircumflex'], 207: ['Idieresis'], 204: ['Igrave'], 300: ['Ibreve'], 304: ['Idotaccent'], 298: ['Imacron'], 302: ['Iogonek'], 296: ['Itilde'], 308: ['Jcircumflex'], 306: ['IJ'], 310: ['Kcommaaccent'], 313: ['Lacute'], 317: ['Lcaron'], 315: ['Lcommaaccent'], 319: ['Ldot'], 183: ['periodcentered'], 209: ['Ntilde'], 323: ['Nacute'], 327: ['Ncaron'], 325: ['Ncommaaccent'], 330: ['Eng'], 8218: ['quotesinglbase'], 211: ['Oacute'], 212: ['Ocircumflex'], 214: ['Odieresis'], 210: ['Ograve'], 213: ['Otilde'], 334: ['Obreve'], 336: ['Ohungarumlaut'], 733: ['hungarumlaut'], 332: ['Omacron'], 340: ['Racute'], 344: ['Rcaron'], 342: ['Rcommaaccent'], 352: ['Scaron'], 346: ['Sacute'], 350: ['Scedilla'], 348: ['Scircumflex'], 536: ['uni0218'], 358: ['Tbar'], 356: ['Tcaron'], 354: ['uni0162'], 538: ['uni021A'], 218: ['Uacute'], 219: ['Ucircumflex'], 220: ['Udieresis'], 217: ['Ugrave'], 364: ['Ubreve'], 368: ['Uhungarumlaut'], 362: ['Umacron'], 370: ['Uogonek'], 366: ['Uring'], 360: ['Utilde'], 7810: ['Wacute'], 372: ['Wcircumflex'], 7812: ['Wdieresis'], 7808: ['Wgrave'], 221: ['Yacute'], 376: ['Ydieresis'], 374: ['Ycircumflex'], 7922: ['Ygrave'], 381: ['Zcaron'], 377: ['Zacute'], 379: ['Zdotaccent'], 225: ['aacute'], 226: ['acircumflex'], 228: ['adieresis'], 224: ['agrave'], 229: ['aring'], 227: ['atilde'], 259: ['abreve'], 257: ['amacron'], 261: ['aogonek'], 231: ['ccedilla'], 263: ['cacute'], 269: ['ccaron'], 265: ['ccircumflex'], 267: ['cdotaccent'], 271: ['dcaron'], 273: ['dcroat'], 233: ['eacute'], 234: ['ecircumflex'], 235: ['edieresis'], 232: ['egrave'], 277: ['ebreve'], 283: ['ecaron'], 279: ['edotaccent'], 275: ['emacron'], 281: ['eogonek'], 287: ['gbreve'], 285: ['gcircumflex'], 291: ['gcommaaccent'], 289: ['gdotaccent'], 295: ['hbar'], 293: ['hcircumflex'], 237: ['iacute'], 238: ['icircumflex'], 239: ['idieresis'], 236: ['igrave'], 301: ['ibreve'], 299: ['imacron'], 303: ['iogonek'], 297: ['itilde'], 309: ['jcircumflex'], 307: ['ij'], 311: ['kcommaaccent'], 312: ['kgreenlandic'], 314: ['lacute'], 318: ['lcaron'], 316: ['lcommaaccent'], 320: ['ldot'], 241: ['ntilde'], 324: ['nacute'], 328: ['ncaron'], 326: ['ncommaaccent'], 331: ['eng'], 329: ['napostrophe'], 243: ['oacute'], 244: ['ocircumflex'], 246: ['odieresis'], 242: ['ograve'], 245: ['otilde'], 335: ['obreve'], 337: ['ohungarumlaut'], 333: ['omacron'], 341: ['racute'], 345: ['rcaron'], 343: ['rcommaaccent'], 353: ['scaron'], 347: ['sacute'], 351: ['scedilla'], 349: ['scircumflex'], 537: ['uni0219'], 359: ['tbar'], 357: ['tcaron'], 355: ['uni0163'], 539: ['uni021B'], 250: ['uacute'], 251: ['ucircumflex'], 252: ['udieresis'], 249: ['ugrave'], 365: ['ubreve'], 369: ['uhungarumlaut'], 363: ['umacron'], 371: ['uogonek'], 367: ['uring'], 361: ['utilde'], 7811: ['wacute'], 373: ['wcircumflex'], 7813: ['wdieresis'], 7809: ['wgrave'], 253: ['yacute'], 255: ['ydieresis'], 375: ['ycircumflex'], 7923: ['ygrave'], 382: ['zcaron'], 378: ['zacute'], 380: ['zdotaccent'], 44: ['comma'], 58: ['colon'], 59: ['semicolon'], 8230: ['ellipsis'], 63: ['question'], 33: ['exclam'], 191: ['questiondown'], 161: ['exclamdown'], 40: ['parenleft'], 41: ['parenright'], 123: ['braceleft'], 125: ['braceright'], 91: ['bracketleft'], 93: ['bracketright'], 47: ['slash'], 92: ['backslash'], 95: ['underscore'], 45: ['hyphen'], 173: ['uni00AD'], 8211: ['endash'], 8212: ['emdash'], 166: ['brokenbar'], 124: ['bar'], 171: ['guillemotleft'], 187: ['guillemotright'], 8249: ['guilsinglleft'], 8250: ['guilsinglright'], 8222: ['quotedblbase'], 8216: ['quoteleft'], 8220: ['quotedblleft'], 8217: ['quoteright'], 8221: ['quotedblright'], 39: ['quotesingle'], 34: ['quotedbl'], 8226: ['bullet'], 38: ['ampersand'], 182: ['paragraph'], 8224: ['dagger'], 8225: ['daggerdbl'], 167: ['section'], 42: ['asterisk'], 8482: ['trademark'], 174: ['registered'], 169: ['copyright'], 64: ['at'], 170: ['ordfeminine'], 186: ['ordmasculine'], 35: ['numbersign'], 8364: ['Euro'], 36: ['dollar'], 165: ['yen'], 163: ['sterling'], 402: ['florin'], 162: ['cent'], 8381: ['uni20BD'], 164: ['currency'], 48: ['zero'], 49: ['one'], 50: ['two'], 51: ['three'], 52: ['four'], 53: ['five'], 54: ['six'], 55: ['seven'], 56: ['eight'], 57: ['nine'], 176: ['degree'], 37: ['percent'], 8240: ['perthousand'], 8260: ['fraction'], 8725: ['uni2215'], 8304: ['zerosuperior'], 185: ['onesuperior'], 178: ['twosuperior'], 179: ['threesuperior'], 8308: ['foursuperior'], 8309: ['fivesuperior'], 8310: ['sixsuperior'], 8311: ['sevensuperior'], 8312: ['eightsuperior'], 8313: ['ninesuperior'], 8320: ['zeroinferior'], 8321: ['oneinferior'], 8322: ['twoinferior'], 8323: ['threeinferior'], 8324: ['fourinferior'], 8325: ['fiveinferior'], 8326: ['sixinferior'], 8327: ['seveninferior'], 8328: ['eightinferior'], 8329: ['nineinferior'], 189: ['onehalf'], 8531: ['onethird'], 188: ['onequarter'], 8539: ['oneeighth'], 8532: ['twothirds'], 190: ['threequarters'], 8540: ['threeeighths'], 8541: ['fiveeighths'], 8542: ['seveneighths'], 43: ['plus'], 8722: ['minus'], 215: ['multiply'], 247: ['divide'], 61: ['equal'], 172: ['logicalnot'], 126: ['asciitilde'], 60: ['less'], 62: ['greater'], 177: ['plusminus'], 94: ['asciicircum'], 8800: ['notequal'], 8776: ['approxequal'], 8804: ['lessequal'], 8805: ['greaterequal'], 8734: ['infinity'], 9674: ['lozenge'], 8730: ['radical'], 8747: ['integral'], 8706: ['partialdiff'], 8719: ['product'], 181: ['mu'], 8721: ['summation'], 8486: ['Omega'], 8710: ['Delta'], 960: ['pi'], 8467: ['uni2113'], 8592: ['arrowleft'], 8593: ['arrowup'], 8594: ['arrowright'], 8595: ['arrowdown'], 8596: ['uni2194'], 8597: ['uni2195'], 8598: ['uni2196'], 8599: ['uni2197'], 8600: ['uni2198'], 8601: ['uni2199'], 8494: ['estimated'], 1040: ['afii10017'], 1041: ['afii10018'], 1042: ['afii10019'], 1043: ['afii10020'], 1044: ['afii10021'], 1045: ['afii10022'], 1025: ['afii10023'], 1046: ['afii10024'], 1047: ['afii10025'], 1048: ['afii10026'], 1049: ['afii10027'], 1050: ['afii10028'], 1051: ['afii10029'], 1052: ['afii10030'], 1053: ['afii10031'], 1054: ['afii10032'], 1055: ['afii10033'], 1056: ['afii10034'], 1057: ['afii10035'], 1058: ['afii10036'], 1059: ['afii10037'], 1060: ['afii10038'], 1061: ['afii10039'], 1062: ['afii10040'], 1063: ['afii10041'], 1064: ['afii10042'], 1065: ['afii10043'], 1066: ['afii10044'], 1068: ['afii10046'], 1067: ['afii10045'], 1069: ['afii10047'], 1070: ['afii10048'], 1071: ['afii10049'], 1027: ['afii10052'], 1168: ['afii10050'], 1026: ['afii10051'], 1028: ['afii10053'], 1029: ['afii10054'], 1024: ['uni0400'], 1030: ['afii10055'], 1031: ['afii10056'], 1032: ['afii10057'], 1037: ['uni040D'], 1036: ['afii10061'], 1033: ['afii10058'], 1034: ['afii10059'], 1035: ['afii10060'], 1038: ['afii10062'], 1039: ['afii10145'], 1122: ['afii10146'], 1138: ['afii10147'], 1140: ['afii10148'], 1072: ['afii10065'], 1073: ['afii10066'], 1074: ['afii10067'], 1075: ['afii10068'], 1076: ['afii10069'], 1077: ['afii10070'], 1105: ['afii10071'], 1078: ['afii10072'], 1079: ['afii10073'], 1080: ['afii10074'], 1081: ['afii10075'], 1082: ['afii10076'], 1083: ['afii10077'], 1084: ['afii10078'], 1085: ['afii10079'], 1086: ['afii10080'], 1087: ['afii10081'], 1088: ['afii10082'], 1089: ['afii10083'], 1090: ['afii10084'], 1091: ['afii10085'], 1092: ['afii10086'], 1093: ['afii10087'], 1094: ['afii10088'], 1095: ['afii10089'], 1096: ['afii10090'], 1097: ['afii10091'], 1098: ['afii10092'], 1100: ['afii10094'], 1099: ['afii10093'], 1101: ['afii10095'], 1102: ['afii10096'], 1103: ['afii10097'], 1107: ['afii10100'], 1169: ['afii10098'], 1106: ['afii10099'], 1108: ['afii10101'], 1109: ['afii10102'], 1104: ['uni0450'], 1110: ['afii10103'], 1111: ['afii10104'], 1112: ['afii10105'], 1117: ['uni045D'], 1116: ['afii10109'], 1113: ['afii10106'], 1114: ['afii10107'], 1115: ['afii10108'], 1118: ['afii10110'], 1119: ['afii10193'], 1123: ['afii10194'], 1139: ['afii10195'], 1141: ['afii10196'], 700: ['uni02BC']}
	testGroups = {'public.kern1.A': ('A', 'Aacute', 'Acircumflex', 'Adieresis', 'Agrave', 'Aring', 'Atilde', 'Abreve', 'Amacron', 'Aogonek'), 'public.kern1.C': ('C', 'Ccedilla', 'Cacute', 'Ccaron', 'Ccircumflex', 'Cdotaccent'), 'public.kern1.D': ('D', 'Eth', 'Dcaron', 'Dcroat'), 'public.kern1.E': ('E', 'AE', 'OE', 'Eacute', 'Ecircumflex', 'Edieresis', 'Egrave', 'Ebreve', 'Ecaron', 'Edotaccent', 'Emacron', 'Eogonek'), 'public.kern1.G': ('G', 'Gbreve', 'Gcircumflex', 'Gcommaaccent', 'Gdotaccent'), 'public.kern1.J': ('J', 'Jcircumflex', 'IJ'), 'public.kern1.K': ('K', 'Kcommaaccent'), 'public.kern1.L': ('L', 'Lslash', 'Lacute', 'Lcaron', 'Lcommaaccent', 'Ldot'), 'public.kern1.N': ('H', 'I', 'M', 'N', 'Hbar', 'Hcircumflex', 'Iacute', 'Icircumflex', 'Idieresis', 'Igrave', 'Ibreve', 'Idotaccent', 'Imacron', 'Iogonek', 'Itilde', 'Ntilde', 'Nacute', 'Ncaron', 'Ncommaaccent', 'Eng', 'napostrophe.case'), 'public.kern1.O': ('O', 'Oslash', 'Oacute', 'Ocircumflex', 'Odieresis', 'Ograve', 'Otilde', 'Obreve', 'Ohungarumlaut', 'Omacron'), 'public.kern1.R': ('R', 'Racute', 'Rcaron', 'Rcommaaccent'), 'public.kern1.S': ('S', 'Scaron', 'Sacute', 'Scedilla', 'Scircumflex', 'uni0218'), 'public.kern1.T': ('T', 'Tbar', 'Tcaron', 'uni0162', 'uni021A'), 'public.kern1.U': ('U', 'Uacute', 'Ucircumflex', 'Udieresis', 'Ugrave', 'Ubreve', 'Uhungarumlaut', 'Umacron', 'Uogonek', 'Uring', 'Utilde'), 'public.kern1.W': ('W', 'Wacute', 'Wcircumflex', 'Wdieresis', 'Wgrave'), 'public.kern1.Y': ('Y', 'Yacute', 'Ydieresis', 'Ycircumflex', 'Ygrave'), 'public.kern1.Z': ('Z', 'Zcaron', 'Zacute', 'Zdotaccent'), 'public.kern1.a': ('a', 'a.open', 'aacute', 'acircumflex', 'adieresis', 'agrave', 'aring', 'atilde', 'abreve', 'amacron', 'aogonek', 'aacute.open', 'acircumflex.open', 'adieresis.open', 'agrave.open', 'aring.open', 'atilde.open', 'abreve.open', 'amacron.open', 'aogonek.open'), 'public.kern1.afii10020': ('afii10020', 'afii10050'), 'public.kern1.afii10021': ('afii10021', 'afii10040', 'afii10043'), 'public.kern1.afii10022': ('afii10022', 'afii10023', 'uni0400'), 'public.kern1.afii10024': ('afii10024', 'afii10028', 'afii10061'), 'public.kern1.afii10032': ('afii10032', 'afii10047', 'afii10048', 'afii10147'), 'public.kern1.afii10037': ('afii10037', 'afii10062'), 'public.kern1.afii10044': ('afii10044', 'afii10046', 'afii10058', 'afii10059', 'afii10146'), 'public.kern1.afii10068': ('afii10068', 'afii10100'), 'public.kern1.afii10069': ('afii10069', 'afii10088', 'afii10091'), 'public.kern1.afii10070': ('afii10070', 'afii10071', 'uni0450'), 'public.kern1.afii10072': ('afii10072', 'afii10076', 'afii10109'), 'public.kern1.afii10080': ('afii10080', 'afii10086', 'afii10095', 'afii10096', 'afii10195'), 'public.kern1.afii10085': ('afii10085', 'afii10110'), 'public.kern1.afii10092': ('afii10092', 'afii10094', 'afii10106', 'afii10107', 'afii10194'), 'public.kern1.afii10103': ('afii10103', 'afii10104'), 'public.kern1.b': ('b', 'p', 'thorn'), 'public.kern1.c': ('c', 'ccedilla', 'cacute', 'ccaron', 'ccircumflex', 'cdotaccent'), 'public.kern1.colon': ('colon', 'semicolon'), 'public.kern1.d': ('d', 'dcroat'), 'public.kern1.dcaron': ('dcaron', 'lcaron'), 'public.kern1.e': ('e', 'e.open', 'ae', 'oe', 'eacute', 'ecircumflex', 'edieresis', 'egrave', 'ebreve', 'ecaron', 'edotaccent', 'emacron', 'eogonek', 'eacute.open', 'ecircumflex.open', 'edieresis.open', 'egrave.open', 'ebreve.open', 'ecaron.open', 'edotaccent.open', 'emacron.open', 'eogonek.open'), 'public.kern1.f': ('f',), 'public.kern1.g': ('g', 'gbreve', 'gcircumflex', 'gcommaaccent', 'gdotaccent'), 'public.kern1.guilsinglleft': ('guillemotleft', 'guilsinglleft'), 'public.kern1.guilsinglleft.case': ('guillemotleft.case', 'guilsinglleft.case'), 'public.kern1.guilsinglright': ('guillemotright', 'guilsinglright'), 'public.kern1.guilsinglright.case': ('guillemotright.case', 'guilsinglright.case'), 'public.kern1.hyphen': ('hyphen', 'endash', 'emdash'), 'public.kern1.hyphen.case': ('hyphen.case', 'endash.case', 'emdash.case'), 'public.kern1.i': ('i', 'fi', 'iacute', 'icircumflex', 'idieresis', 'igrave', 'ibreve', 'i.dot', 'imacron', 'iogonek', 'itilde'), 'public.kern1.j': ('j', 'dotlessj', 'jcircumflex', 'ij'), 'public.kern1.k': ('k', 'kcommaaccent', 'kgreenlandic'), 'public.kern1.n': ('h', 'm', 'n', 'hbar', 'hcircumflex', 'ntilde', 'nacute', 'ncaron', 'ncommaaccent', 'napostrophe'), 'public.kern1.o': ('o', 'oslash', 'oacute', 'ocircumflex', 'odieresis', 'ograve', 'otilde', 'obreve', 'ohungarumlaut', 'omacron'), 'public.kern1.period': ('period', 'comma', 'ellipsis'), 'public.kern1.quoteleft': ('quoteleft', 'quotedblleft'), 'public.kern1.quoteright': ('quoteright', 'quotedblright'), 'public.kern1.quotesinglbase': ('quotesinglbase', 'quotedblbase'), 'public.kern1.quotesingle': ('quotesingle', 'quotedbl'), 'public.kern1.r': ('r', 'racute', 'rcaron', 'rcommaaccent'), 'public.kern1.s': ('s', 'scaron', 'sacute', 'scedilla', 'scircumflex', 'uni0219'), 'public.kern1.t': ('t', 'tbar', 'uni0163', 'uni021B'), 'public.kern1.u': ('g.open', 'u', 'gbreve.open', 'gcircumflex.open', 'gcommaaccent.open', 'gdotaccent.open', 'uacute', 'ucircumflex', 'udieresis', 'ugrave', 'ubreve', 'uhungarumlaut', 'umacron', 'uogonek', 'uring', 'utilde'), 'public.kern1.w': ('w', 'wacute', 'wcircumflex', 'wdieresis', 'wgrave'), 'public.kern1.y': ('y', 'yacute', 'ydieresis', 'ycircumflex', 'ygrave'), 'public.kern1.z': ('z', 'zcaron', 'zacute', 'zdotaccent'), 'public.kern2.A': ('A', 'Aacute', 'Acircumflex', 'Adieresis', 'Agrave', 'Aring', 'Atilde', 'Abreve', 'Amacron', 'Aogonek'), 'public.kern2.J': ('J', 'Jcircumflex'), 'public.kern2.N': ('B', 'D', 'E', 'F', 'H', 'I', 'K', 'L', 'M', 'N', 'P', 'R', 'Thorn', 'Dcaron', 'Dcroat', 'Eacute', 'Ecircumflex', 'Edieresis', 'Egrave', 'Ebreve', 'Ecaron', 'Edotaccent', 'Emacron', 'Eogonek', 'Hbar', 'Hcircumflex', 'Iacute', 'Icircumflex', 'Idieresis', 'Igrave', 'Ibreve', 'Idotaccent', 'Imacron', 'Iogonek', 'Itilde', 'IJ', 'Kcommaaccent', 'kgreenlandic.case', 'Lacute', 'Lcaron', 'Lcommaaccent', 'Ldot', 'Ntilde', 'Nacute', 'Ncaron', 'Ncommaaccent', 'Eng', 'napostrophe.case', 'Racute', 'Rcaron', 'Rcommaaccent'), 'public.kern2.O': ('C', 'G', 'O', 'Q', 'OE', 'Oslash', 'Ccedilla', 'Cacute', 'Ccaron', 'Ccircumflex', 'Cdotaccent', 'Gbreve', 'Gcircumflex', 'Gcommaaccent', 'Gdotaccent', 'Oacute', 'Ocircumflex', 'Odieresis', 'Ograve', 'Otilde', 'Obreve', 'Ohungarumlaut', 'Omacron'), 'public.kern2.S': ('S', 'Scaron', 'Sacute', 'Scedilla', 'Scircumflex', 'uni0218'), 'public.kern2.T': ('T', 'Tbar', 'Tcaron', 'uni0162', 'uni021A'), 'public.kern2.U': ('U', 'Uacute', 'Ucircumflex', 'Udieresis', 'Ugrave', 'Ubreve', 'Uhungarumlaut', 'Umacron', 'Uogonek', 'Uring', 'Utilde'), 'public.kern2.W': ('W', 'Wacute', 'Wcircumflex', 'Wdieresis', 'Wgrave'), 'public.kern2.Y': ('Y', 'Yacute', 'Ydieresis', 'Ycircumflex', 'Ygrave'), 'public.kern2.Z': ('Z', 'Zcaron', 'Zacute', 'Zdotaccent'), 'public.kern2.a': ('a', 'a.open', 'ae', 'aacute', 'acircumflex', 'adieresis', 'agrave', 'aring', 'atilde', 'abreve', 'amacron', 'aogonek', 'aacute.open', 'acircumflex.open', 'adieresis.open', 'agrave.open', 'aring.open', 'atilde.open', 'abreve.open', 'amacron.open', 'aogonek.open'), 'public.kern2.afii10029': ('afii10029', 'afii10058'), 'public.kern2.afii10032': ('afii10032', 'afii10035', 'afii10053', 'afii10147'), 'public.kern2.afii10036': ('afii10036', 'afii10044', 'afii10051', 'afii10060'), 'public.kern2.afii10037': ('afii10037', 'afii10062'), 'public.kern2.afii10077': ('afii10077', 'afii10106'), 'public.kern2.afii10079': ('afii10079', 'afii10067', 'afii10068', 'afii10074', 'afii10075', 'afii10076', 'afii10078', 'afii10081', 'afii10088', 'afii10090', 'afii10091', 'afii10093', 'afii10094', 'afii10096', 'afii10100', 'afii10098', 'uni045D', 'afii10109', 'afii10107', 'afii10193'), 'public.kern2.afii10080': ('afii10080', 'afii10070', 'afii10071', 'afii10083', 'afii10086', 'afii10101', 'uni0450', 'afii10195'), 'public.kern2.afii10084': ('afii10084', 'afii10092'), 'public.kern2.afii10085': ('afii10085', 'afii10110'), 'public.kern2.afii10103': ('afii10103', 'afii10104'), 'public.kern2.colon': ('colon', 'semicolon'), 'public.kern2.d': ('d', 'q', 'eth', 'dcaron', 'dcroat'), 'public.kern2.f': ('f', 'germandbls', 'fi', 'fl'), 'public.kern2.g': ('g', 'gbreve', 'gcircumflex', 'gcommaaccent', 'gdotaccent'), 'public.kern2.guilsinglleft': ('guillemotleft', 'guilsinglleft'), 'public.kern2.guilsinglleft.case': ('guillemotleft.case', 'guilsinglleft.case'), 'public.kern2.guilsinglright': ('guillemotright', 'guilsinglright'), 'public.kern2.guilsinglright.case': ('guillemotright.case', 'guilsinglright.case'), 'public.kern2.h': ('h', 'k', 'l', 'hcircumflex', 'kcommaaccent', 'lacute', 'lcaron', 'lcommaaccent'), 'public.kern2.hyphen': ('hyphen', 'endash', 'emdash'), 'public.kern2.hyphen.case': ('hyphen.case', 'endash.case', 'emdash.case'), 'public.kern2.i': ('i', 'iacute', 'icircumflex', 'idieresis', 'igrave', 'ibreve', 'i.dot', 'imacron', 'iogonek', 'itilde', 'ij'), 'public.kern2.j': ('j', 'dotlessj', 'jcircumflex'), 'public.kern2.n': ('m', 'n', 'p', 'r', 'dotlessi', 'kgreenlandic', 'ntilde', 'nacute', 'ncaron', 'ncommaaccent', 'eng', 'racute', 'rcaron', 'rcommaaccent'), 'public.kern2.o': ('c', 'e', 'e.open', 'g.open', 'o', 'oe', 'oslash', 'ccedilla', 'cacute', 'ccaron', 'ccircumflex', 'cdotaccent', 'eacute', 'ecircumflex', 'edieresis', 'egrave', 'ebreve', 'ecaron', 'edotaccent', 'emacron', 'eogonek', 'eacute.open', 'ecircumflex.open', 'edieresis.open', 'egrave.open', 'ebreve.open', 'ecaron.open', 'edotaccent.open', 'emacron.open', 'eogonek.open', 'gbreve.open', 'gcircumflex.open', 'gcommaaccent.open', 'gdotaccent.open', 'oacute', 'ocircumflex', 'odieresis', 'ograve', 'otilde', 'obreve', 'ohungarumlaut', 'omacron'), 'public.kern2.period': ('period', 'comma', 'ellipsis'), 'public.kern2.quoteleft': ('quoteleft', 'quotedblleft'), 'public.kern2.quoteright': ('quoteright', 'quotedblright'), 'public.kern2.quotesinglbase': ('quotesinglbase', 'quotedblbase'), 'public.kern2.quotesingle': ('quotesingle', 'quotedbl'), 'public.kern2.s': ('s', 'scaron', 'sacute', 'scedilla', 'scircumflex', 'uni0219'), 'public.kern2.t': ('t', 'tbar', 'tcaron', 'uni0163', 'uni021B'), 'public.kern2.u': ('u', 'uacute', 'ucircumflex', 'udieresis', 'ugrave', 'ubreve', 'uhungarumlaut', 'umacron', 'uogonek', 'uring', 'utilde'), 'public.kern2.w': ('w', 'wacute', 'wcircumflex', 'wdieresis', 'wgrave'), 'public.kern2.y': ('y', 'yacute', 'ydieresis', 'ycircumflex', 'ygrave'), 'public.kern2.z': ('z', 'zcaron', 'zacute', 'zdotaccent')}
	testFont = RFont()
	for g in testGlyphs:
		testFont.newGlyph(g)
	for u, gnames in testCmap.items():
		for g in gnames:
			unis = list(testFont[g].unicodes)
			unis.append(u)
			testFont[g].unicodes = unis
	for gr, members in testGroups.items():
		testFont.groups[gr] = members
	doctest.testmod()
