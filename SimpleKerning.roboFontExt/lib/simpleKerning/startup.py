from mojo.roboFont import RFont
from ufo2ft.featureWriters.kernFeatureWriter import unicodeBidiType
from constants import RTL_KEY

def getRTLMap(font):
	rtl = 0
	rtlMap = {}
	glyphOrder = [g for g in font.glyphOrder if g in font]
	for gName in glyphOrder:
		g = font[gName]
		if g.unicodes != ():
			if unicodeBidiType(g.unicodes[0]) == "R":
				rtl = 1
			else:
				rtl = 0
		rtl = font[gName].lib.get(RTL_KEY, rtl)
		rtlMap[gName] = rtl
	return rtlMap

RFont.rtlMap = property(lambda self: getRTLMap(self))
