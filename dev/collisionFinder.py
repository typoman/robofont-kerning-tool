from drawBot import *
from mojo.roboFont import RGlyph
import itertools
from simpleKerning import RTL_KEY

def getGlyphBezierPath(glyph, strokeWidth=0):
	nspath = glyph.naked().getRepresentation("defconAppKit.NSBezierPath")
	b = BezierPath()
	b.setNSBezierPath(nspath)
	if strokeWidth != 0:
		newPath = b.expandStroke(strokeWidth).removeOverlap()
		return newPath.union(b)
	return b.removeOverlap()

RGlyph.getBezierPath = getGlyphBezierPath

def getBezierPathsIntersecion(b1, b2):
	intersect = b2.intersection(b1)
	intersectBounds = intersect.bounds()
	if intersectBounds is not None:
		return intersect

def getPairCombinations(pair, groups):
	"""
	Gives all the possible combinations a pair with kerning groups can create
	"""
	left, right = pair
	leftGlyphs = groups.get(left, [left])
	rightGlyphs = groups.get(right, [right])
	return list(itertools.product(leftGlyphs, rightGlyphs))

def kerningPairIsRTL(f, rtlMap, kerningPair):
	for g in kerningPair:
		if f[g].lib.get(RTL_KEY, rtlMap[g]) == 1:
			return True
	return False

def getKerningPairOverlaps(f, kerningPair, rtlMap={}, minDistance=0):
	allCombinations = getPairCombinations(kerningPair, f.groups)
	value = f.kerning.get(kerningPair, 0)
	RTL = False
	path = f.path
	overlappingGlyphs1 = set()
	overlappingGlyphs2 = set()
	if kerningPairIsRTL(f, rtlMap, allCombinations[0]):
		RTL = True
	for p in allCombinations:
		e1, e2 = p
		g1, g2 = f[e1], f[e2]
		b1, b2 = g1.getBezierPath(minDistance), g2.getBezierPath(minDistance)
		if RTL:
			b2.translate(-(g2.width+value))
		else:
			b2.translate(g1.width+value)
		intersectionPath = getBezierPathsIntersecion(b1, b2)
		if intersectionPath is not None:
			overlappingGlyphs1.add(g1)
			overlappingGlyphs2.add(g2)
	if overlappingGlyphs1 or overlappingGlyphs2:
		# if overlap glyphs are half the size of the group, make a new group
		# if overlap glyphs are too little make exceptions per glyph
		# if overlap glyphs are exactly size of the group, make it a group
		# iterate over the all the combos to change the kerning
		# add a value based on width of the overlap
		print("overlaps first entry:")
		print(' '.join(overlappingGlyphs1))
		print("overlaps second entry:")
		print(' '.join(overlappingGlyphs2))
	else:
		print("No overlaps")

def drawOverlap(path, p, b1, b2, intersectionPath=None):
	newDrawing()
	size(w, h)
	translate(w/2, h/2)
	fill(0)
	stroke(None)
	drawPath(b1)
	drawPath(b2)
	fill(None)
	lineDash(30, 30)
	stroke(1, 0, 0, 1)
	strokeWidth(10)
	# oval(cx - 200, cy - 200, 400, 400)
	# fill(1, 0, 0, 1)
	if intersectionPath is not None:
		drawPath(intersectionPath)
	saveImage(f"{path}/collisions/{p[0]}_{p[1]}.pdf")

if __name__ == '__main__':
	from datetime import datetime
	import subprocess
	import os

	from simpleKerning.kernWindow import SimpleKerningWindow

	f = CurrentFont()


	upm = f.info.unitsPerEm
	w, h = upm * 3, upm * 3
	os.makedirs(f.path+"/collisions", exist_ok=True)
	getKerningPairOverlaps(f, ('public.kern1.reh.fina','public.kern2.b_y.init'), f.rtlMap)
	k = SimpleKerningWindow()
	k.setFont(f)
	k.setPairList(sorted(f.kerning.keys()))

	# fn = os.path.split(os.path.realpath(__file__))[1] + datetime.now().strftime("%Y|%m|%d-%H%M%S")
	# saveImage(f"{fn}.pdf")
	# subprocess.call(['open', "%s.pdf" %fn])
