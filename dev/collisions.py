from shaper import ShapeUFO
import logging
logger = logging.getLogger(__name__)

"""
- Move the dot forward for the prevGlyph collision
	- If it collides with following glyph add kerning to the following glyphs
- Move the dot backward for the front collision
	- If it collides with previous glyph add kerning to the previous glyphs
- Compress collisions into groups by:
	- Rounding the movement value
	- Finding similar shapes in marks and base letters
"""

class Collision():
	"""
	`glyph` is the main glyph that can be moved or replaced in the context.
	"""

	def __init__(self, glyph, prevContext=None, afterContext=None, referenceWord=None,
		intersectPath=None):
		self.glyph = glyph
		self.prevContext = prevContext
		self.afterContext = afterContext
		self.referenceWord = referenceWord
		self.intersectPath = intersectPath

	def setContext(self, index, glyphSequence):
		"""
		if index = 0 the context is prevContext
		if index = 1 the context is afterContext
		"""
		if index == 0:
			self.prevContext = tuple(glyphSequence)
		elif index == 1:
			self.afterContext = tuple(glyphSequence)

	def __hash__(self):
		return hash((self.glyph.name, self.prevContext, self.afterContext))

	def __repr__(self):
		return self.referenceWord.string

class Word():
	"""
	Just a simple container for the word and glyph run data.
	"""

	def __init__(self, string, glyphRun=None):
		self.string = string
		self.glyphRun = glyphRun

	def __hash__(self):
		return hash(self.string)

	def __repr__(self):
		return self.string

class CollisionFinder():

	def __init__(self, fontPath, minDistance=0, maxContext=3):
		"""
		minDistance: adds a stroke around mark glyphs to ensure a minimum distance
		between the glyphs.
		maxContext: how many glyphs before the mark or after the mark should be
		checked for the collision with the current mark.
		"""
		self._shaper = ShapeUFO(fontPath)
		self.marks = self._shaper.glyphTypes["mark"]
		self._collisionsDict = {}
		self.maxContext = maxContext
		self._shaper.expandGlyphSetStrokes(minDistance, self.marks)

	def _collectCollisionsInPrevContext(self):
		# reverse the prev context to iterate over them from the closest
		# glyph to the center
		prevContextReversed = list(reversed(self._prevContextLogicalOrder))
		for glyphIndexInContext, collidingGlyph in enumerate(prevContextReversed):
			contextGlyphPath = collidingGlyph.bezierPath
			intersect = self._currentMarkGlyphPath.intersection(contextGlyphPath)
			intersectBounds = intersect.bounds()
			if intersectBounds is not None:
				c = Collision(self._glyphToMove)
				contextGlyphs = prevContextReversed[:glyphIndexInContext+1]
				# print(self._glyphToMove.name, list(reversed(contextGlyphs)), self._currentWord.string, collidingGlyph.name)
				c.setContext(0, list(reversed(contextGlyphs)))
				c.referenceWord = self._currentWord
				c.intersectPath = intersect
				self._collisionsDict.setdefault(self._glyphToMove.name, set()).add(c)
				return

	def _collectCollisionsInNextContext(self):
		for glyphIndexInContext, collidingGlyph in enumerate(self._nextContextLogicalOrder):
			contextGlyphPath = collidingGlyph.bezierPath
			intersect = self._currentMarkGlyphPath.intersection(contextGlyphPath)
			intersectBounds = intersect.bounds()
			if intersectBounds is not None:
				c = Collision(self._glyphToMove)
				contextGlyphs = self._nextContextLogicalOrder[:glyphIndexInContext+1]
				# print("n", self._currentWord.string, contextGlyphs)
				c.setContext(1, contextGlyphs)
				c.referenceWord = self._currentWord
				c.intersectPath = intersect
				self._collisionsDict.setdefault(self._glyphToMove.name, set()).add(c)
				return

	def getCollisionsInWord(self, word):
		self._collisionsDict = {}
		glyphRun = self._shaper.shape(word)
		self._currentWord = Word(word, glyphRun)
		for i, self._glyphToMove in enumerate(glyphRun):
			if self._glyphToMove.name in self.marks:
				if self._glyphToMove.strokedPath is not None:
					self._currentMarkGlyphPath = self._glyphToMove.strokedPath
				else:
					self._currentMarkGlyphPath = self._glyphToMove.bezierPath
				self._nextContextLogicalOrder = glyphRun[:i][i-self.maxContext:][::-1]
				self._prevContextLogicalOrder = glyphRun[i+2:][:self.maxContext][::-1]
				logger.debug(self._prevContextLogicalOrder, self._glyphToMove, self._nextContextLogicalOrder)
				self._collectCollisionsInPrevContext()
				self._collectCollisionsInNextContext()
		return self._collisionsDict

if __name__ == '__main__':
	words = []
	with open("testWords_1.txt", "rt", encoding="utf-8") as f:
		txt = f.read()
		words.extend(txt.split("\n"))

	cf = CollisionFinder(
		"/Users/bman/git/private/tajrish/Masters/1- Arabic/Tajrish-Black.ufo",
		25
		)

	for word in words:
		cf.getCollisionsInWord(word)
