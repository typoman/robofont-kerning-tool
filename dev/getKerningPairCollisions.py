import sys
import time
import itertools
import multiprocessing
import booleanOperations
from booleanOperations.booleanGlyph import BooleanGlyph
from fontParts.world import RFont, RGlyph
from fontTools.pens.boundsPen import BoundsPen
from functools import lru_cache

"""
- option to save flattened glyphs as a cache in tmp folder. Make sure to keep track of changes.
"""

@lru_cache(maxsize=None)
def getBooleanGlyph(glyphName, shift):
    g = f[glyphName]
    if shift != 0:
        shiftedGlyph = RGlyph()
        shiftedGlyph.appendGlyph(g)
        shiftedGlyph.moveBy((shift, 0))
        bg = BooleanGlyph(shiftedGlyph)
    else:
        bg = BooleanGlyph(g)
    bg.width = g.width
    return bg

def getOverlapsForKerningPair(kerningPair):
    allCombinations = getPairCombinations(kerningPair, groups)
    for p in allCombinations:
        kernValue = f.kerning.find(p, 0)
        gname1, gname2 = p
        g1 = f[gname1]
        g2 = f[gname2]
        if RTL:
            shift = -g2.width - kernValue
        else:
            shift = g1.width + kernValue
        bg1 = getBooleanGlyph(gname1, 0)
        bg2 = getBooleanGlyph(gname2, shift)
        overlapG = bg1.intersection(bg2)
        overlapBounds = getBounds(overlapG)
        if overlapBounds is not None:
            bottomX, bottomY, topX, topY = overlapBounds
            distance = round(topX-bottomX, -1)  # round the distance to 10
            return kerningPair, kernValue + distance

def getBounds(g):
    boundsPen = BoundsPen(f)
    g.draw(boundsPen)
    return boundsPen.bounds

def getPairCombinations(pair, groups):
    """
    Gives all the possible combinations a pair with kerning groups can create
    """
    left, right = pair
    leftGlyphs = groups.get(left, [left])
    rightGlyphs = groups.get(right, [right])
    return list(itertools.product(leftGlyphs, rightGlyphs))

def flatenGlyph(g):
    """
    Decomposes and removeOverlaps
    """
    g.decompose()
    contours = list(g.contours)
    g.clear(contours=True, components=False,
            anchors=False, guidelines=False, image=False)
    booleanOperations.union(contours, g.getPointPen())

RTL = True
ufoPath = sys.argv[-1]
start = time.time()
f = RFont(ufoPath)
for g in f:
    flatenGlyph(g)
start = time.time()
groups = dict(f.groups)
kerning = f.kerning
items = list(f.kerning.keys())
firstGroups = [g for g in groups if g.startswith('public.kern1.')]
secondGroups = [g for g in groups if g.startswith('public.kern2.')]
items.extend(itertools.product(firstGroups, secondGroups))
pool = multiprocessing.Pool()
result = pool.map(
    getOverlapsForKerningPair, items)
pool.close()
newPairs = [p for p in result if p]
print(newPairs)
