import asyncio
import sys

f = CurrentFont()
async def runOtherScript():
    print("Start")
    proc = await asyncio.create_subprocess_exec(sys.executable, "getKerningPairCollisions.py" , f.path,
    stdout=asyncio.subprocess.PIPE,
    stderr=asyncio.subprocess.PIPE)
    stdout, stderr = await proc.communicate()
    print(eval(stdout))
    print("End")

async def main():
    await asyncio.gather(runOtherScript())

if __name__ == "__main__":
    asyncio.create_task(main())
