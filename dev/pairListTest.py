from simpleKerning import constants
from simpleKerning import pairListBuilder
from simpleKerning.startup import *
import simpleKerning
from importlib import reload
reload(constants)
reload(simpleKerning)
reload(pairListBuilder)
from simpleKerning.pairListBuilder import *
from simpleKerning.kernWindow import SimpleKerningWindow

f = CurrentFont()
f = CurrentFont()
k = SimpleKerningWindow()
k.setFont(f)

apl = ArabicPairList(f)
k.setPairList(sorted(apl.letters))

