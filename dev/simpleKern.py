from simpleKerning import constants
from simpleKerning import kernWindow
from simpleKerning.startup import *
import simpleKerning
from importlib import reload
reload(constants)
reload(simpleKerning)
reload(kernWindow)
from simpleKerning.kernWindow import SimpleKerningWindow
f = CurrentFont()
k = SimpleKerningWindow()
k.setFont(f)
k.setPairList(sorted(f.kerning.keys()))
