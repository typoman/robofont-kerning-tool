from simpleKerning.kernWindow import SimpleKerningWindow

f1 = AllFonts()[0]
pairs1 = set(f1.kerning.keys())
newKerning = {}
newPairs = set()
for f in AllFonts()[1:]:
	thisPairs = set(f.kerning.keys())
	newPairs.update(thisPairs ^ pairs1)
	# for p in newPairs:
	# 	if p not in newKerning:
	# 		newKerning[p] = f.kerning.find(p, 0)

# for f in AllFonts():
# 	for p in newKerning:
# 		if p not in f.kerning:
# 			f.kerning[p] = newKerning[p]

f = CurrentFont()
k = SimpleKerningWindow()
k.setFont(f)
k.setPairList(sorted(newPairs))

